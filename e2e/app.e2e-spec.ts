import { GalaxyUiPage } from './app.po';

describe('galaxy-ui App', function() {
  let page: GalaxyUiPage;

  beforeEach(() => {
    page = new GalaxyUiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
