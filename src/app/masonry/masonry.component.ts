import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {MasonryOptions} from './masonry_options'

declare var Masonry: any;

@Component({
  moduleId: module.id,
  selector: '[masonry],masonry',
  template: '<ng-content></ng-content>',
})

export class MasonryComponent implements OnInit {

  private _masonry = null;

  @Input() public options: MasonryOptions;

  constructor(
    private _element: ElementRef
  ) {
  }

  ngOnInit(): any {
    //Create masonry options object
    if (!this.options) this.options = {};

    // Set default itemSelector
    if (!this.options.itemSelector) {
      this.options.itemSelector = '[masonry-brick], masonry-brick';
    }

    // Set element display to block
    if (this._element.nativeElement.tagName === 'MASONRY') {
      this._element.nativeElement.style.display = 'block';
    }

    // Initialize Masonry
    this._masonry = new Masonry(this._element.nativeElement, this.options);
    // this._msnry = new Masonry(this._element.nativeElement, this.options);

     //console.log('AngularMasonry:', 'Initialized');
  }


  public layout() {
    setTimeout(() => {
      this._masonry.layout();
    });

    //console.log('AngularMasonry:', 'Layout');
  }

  public add(element, prepend: boolean = false) {
    // Tell Masonry that a child element has been added
    if (prepend) {
      this._masonry.prepend(element);
    }
    else {
      this._masonry.appended(element);
    }

    // Layout items
    this.layout();

    // console.log('AngularMasonry:', 'Brick added');
  }

  public remove(element) {
    // Tell Masonry that a child element has been removed
    this._masonry.remove(element);

    // Layout items
    this.layout();

    // console.log('AngularMasonry:', 'Brick removed');
  }
}
