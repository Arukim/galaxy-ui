import { Directive, Inject, ElementRef,
  forwardRef, OnDestroy, AfterViewInit } from '@angular/core';

import {MasonryComponent} from './'

@Directive({
  selector: '[masonry-brick], masonry-brick'
})

export class MasonryBrickComponent implements OnDestroy, AfterViewInit {

    constructor(
        private _element: ElementRef,
        @Inject(forwardRef(() => MasonryComponent)) private _parent: MasonryComponent
    ) { }

    ngAfterViewInit() {

        // imagesLoaded(this._element.nativeElement, function(instance) {
        //     console.log('all images are loaded');
        // });

        //console.log('brick loaded')

        this._parent.add(this._element.nativeElement);
    }

    ngOnDestroy() {
        this._parent.remove(this._element.nativeElement);
    }
}
