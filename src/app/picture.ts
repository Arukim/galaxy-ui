export class Picture {
  id: number;
  title: string;
  created: string;
  path: string;
}
