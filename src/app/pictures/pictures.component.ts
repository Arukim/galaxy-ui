import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import {Picture} from '../picture';
import {PictureService} from '../picture.service'
import {Observable} from 'rxjs/Observable';
import {MasonryComponent, MasonryBrickComponent} from '../masonry'


// Add the RxJS Observable operators we need in this app.
import '../rxjs-operators';
//import {Masonry} from 'masonry-layout'
//import { MASONRY_DIRECTIVES } from 'angular2-masonry'

declare var Masonry:any;

@Component({
  moduleId: module.id,
  selector: 'app-pictures',
  templateUrl: 'pictures.component.html',
  styleUrls: ['pictures.component.css'],
  directives: [MasonryComponent, MasonryBrickComponent],
})

export class PicturesComponent implements OnInit{
  title = 'Галерея Sony-Club';
  selectedPicture: Picture;
  pictures: Picture[];
  errorMessage: string;

  constructor(private pictureService: PictureService,
    private router: Router
  ) {
    this.pictures = [];

  }

  getPictures() {
    this.pictureService.getPictures()
      .subscribe(
        pictures => {
          for(var id in pictures){
            this.pictures.push(pictures[id])
          }
       },
      error => this.errorMessage = <any>error);
  }

  gotoDetail(pic: Picture) {
    let link = ['/picture', pic.id];
    this.router.navigate(link);
  }

  ngOnInit(): any {
    this.getPictures();
  }

}
