import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

import {Picture} from './picture';
import {Observable} from 'rxjs/Observable';
// Add the RxJS Observable operators we need in this app.
import './rxjs-operators';

import {environment} from './';


@Injectable()
export class PictureService {
  constructor (private http: Http) {}

  getPictures(): Observable<Picture[]> {
    return this.http.get(environment.apiEndpoint + 'pictures')
    .map(this.extractData)
    .catch(this.handleError);
  }

  getPicture(id: string): Observable<Picture> {
    return this.http.get(environment.apiEndpoint + 'pictures/' + id)
    .map(this.extractData)
    .catch(this.handleError);
  }

  private extractData(res: Response){
    let body = res.json();
    return body || {};
  }

  private handleError(error: any){
    let errMsg = (error.message) ? error.message :
    error.status ?  `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
