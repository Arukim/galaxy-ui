import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Picture} from '../picture';
import {PictureService} from '../picture.service'

// Add the RxJS Observable operators we need in this app.
import '../rxjs-operators';

@Component({
  moduleId: module.id,
  selector: 'app-picture-detail',
  templateUrl: 'picture-detail.component.html',
  styleUrls: ['picture-detail.component.css']
})

export class PictureDetailComponent implements OnInit, OnDestroy {
  picture: Picture;
  sub: any;

  constructor(
    private pictureService: PictureService,
    private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let id = params['id'];
      this.pictureService.getPicture(id)
         .subscribe(p => this.picture = p);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  goBack() {
    window.history.back();
  }
}
