import { Component, OnInit } from '@angular/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgStyle} from '@angular/common';
import {FILE_UPLOAD_DIRECTIVES, FileUploader} from 'ng2-file-upload/ng2-file-upload';

import {environment} from '../';

// const URL = '/api/';
const URL = 'pictures';

@Component({
  moduleId: module.id,
  selector: 'app-picture-upload',
  templateUrl: 'picture-upload.component.html',
  styles: [],
  directives: [FILE_UPLOAD_DIRECTIVES, NgClass, NgStyle, CORE_DIRECTIVES, FORM_DIRECTIVES]
})
export class PictureUploadComponent implements OnInit {
  public uploader:FileUploader = new FileUploader({url: environment.apiEndpoint + URL});
  constructor() {}

  ngOnInit() {
  }

}
