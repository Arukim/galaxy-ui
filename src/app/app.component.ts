import {Component} from '@angular/core';

import {ROUTER_DIRECTIVES} from "@angular/router";

import {PictureService} from './picture.service'


@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  directives: [ROUTER_DIRECTIVES],
  providers: [PictureService]
})

export class AppComponent {
  title = 'Галерея Sony-Club';
}
