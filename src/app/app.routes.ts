import {provideRouter, RouterConfig, } from '@angular/router';

import { LocationStrategy,
         HashLocationStrategy } from '@angular/common';
import {PicturesComponent} from './pictures';
import {NotFoundComponent} from './comp/error/not-found'
import {PictureUploadComponent} from './picture-upload';
import {PictureDetailComponent} from './picture-detail';

export const routes: RouterConfig = [
  {path: '', redirectTo: '/pictures', terminal: true },
  {path: 'pictures/upload', component: PictureUploadComponent },
  {path: 'pictures', component: PicturesComponent},
  {path: 'picture/:id', component: PictureDetailComponent},
  {path: 'notFound', component: NotFoundComponent},
  {path: '**', redirectTo: '/notFound', }
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
